
![alt text](img/crisp_logo.png "Crisp")

## Introduction

This is a small system to transform text data. The typical use case
is taking a delimited data file from a customer and transforming it 
to fit a standardized schema.

## Technology used

On Mac/Linux/Windows make sure you have these:
1. Java JDK 8
2. Maven 3+. Below are libraries used in this project managed by Maven :
- Apache Common-Text : library for string processing such as make sure product name has proper case.
- JUnit : framework for testing our data Transformer.

## Instructions 

1. Make sure you have everything in Technology used section above installed in your computer. 
2. Go to directory where the project is and do 
``` java
mvn clean compile
```
You should see this on the output.
```text
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.837 s
[INFO] Finished at: 2020-10-31T13:52:51-07:00
[INFO] ------------------------------------------------------------------------
````
3. Run the project like this below 
``` java
mvn exec:java -Dexec.mainClass=com.crisp.datatransformer.TransformerRunner -Dexec.args=[input_filename]
```
where input_filename is the location of your input file.
Two outputs will be generated : **output.csv** and **error_output.txt** in the same folder of the input file name location.

You should see something like this in the console output.
```text 
Processed 2 data with # of errors 1
```

**Sample data is provided inside sample/sample1.csv**

If you want to run sample just use "runsample" in the inputfilename.
Then the program will use sample/sample1.csv as the input and will generate sample/output.csv and sample/error_output.txt

```java
mvn exec:java -Dexec.mainClass=com.crisp.datatransformer.TransformerRunner -Dexec.args=runsample
```

## Architecture

![alt text](img/architecture_diagram.png "Architecture Diagram")

- `TransformerRunner` is the main entry of this library.

- `TransformerFactory` is responsible to determine what kind
  of `Transformer` will be used to produced our desire output. 
  This folllows **Factory design pattern**.
  For the purpose of this project, there is only currently one type of transformer
  which is `CsvTransformer`.
 - `CsvTransformerConfig` is configuration used for CSV transformer.
 - `TransformerType` is Enum to describes different types of Transformers available.

#### Test suites
- `CsvTransformerTest` is where the test suite is to test CsvTransformer.
- `TransformerRunnerTest` is there the test suite is to test TransformerRunner.

## Configuration file
   
- The configuration can be configured to set the position of fields 
we are interested in the CSV input file. 
- Since we know that all products are measured in "kg", then there's 
also a setting to set the unit that we need.
- We also put the header text to be put in our transformed output in the configuration.

For example if we have data like below: 
```text
"1000,2018,1,1,P-10001,Arugola,5250.50,Lorem,Ipsum,"
```
then we want to set the configuration like this:

```text
columnPosition.orderId=0
columnPosition.orderDate=1,2,3
columnPosition.productId=4
columnPosition.productName=5
columnPosition.quantity=6
transformedData.unit=kg
transformedData.headerText=OrderID,OrderDate,ProductId,ProductName,Quantity,Unit
```

## Assumptions made

1. For the date input, the example of the delimited file 
from customer shows that year, month, and date are taken 
from 3 different fields. We have to assume that this is 
the case for the input.

2. Also for the date input, we don't know whether time is in 
local server time or in a specific timezone.

3. The output that needs to be produced is not clearly specified in the documentation. 
This project assumes to create two different outputs which is:
- Transformed output file in CSV format called output.csv.
- Another file that contains failed transformation along with the reason. The file is called error_output.txt.

## Next steps if this were a real project.
1. Create a REST API for user to integrate with their product.

2. This project uses Factory design pattern, so we can extend this library
   to different types of Transformer besides CSV transformer.
   Example : we can build TXT transformer in the future
   that will be able to accept TXT input.
   
3. Currently this project assumes to produce CSV as the output, if this were a real 
project we can extend it to produce other types of output such as XML or JSON.

