package com.crisp.datatransformer;

/**
 * Transformer interface.
 */
public interface Transformer {
    String transformData(String rawData) throws Exception;
}
