package com.crisp.datatransformer;

import com.crisp.datatransformer.config.CsvTransformerConfig;
import org.apache.commons.text.WordUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Data transformer to transform raw data to the acceptable format.
 */
public class CsvTransformer implements Transformer {

    /**
     * SimpleDateFormat-er used to convert dates.
     */
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    /***
     * Transform data to the proper format.
     *
     * @param rawData   Raw data to be transformed.
     * @return transformed data back to caller.
     */
    public String transformData(String rawData) throws Exception {

        String[] tokens = rawData.split(",(?=([^\"]|\"[^\"]*\")*$)");
        StringBuilder transformedData = new StringBuilder();

        int orderId =
                Integer.valueOf(tokens[CsvTransformerConfig.orderIdColumnPosition]);
        Date orderDate =
                sdf.parse(tokens[CsvTransformerConfig.orderYearColumnPosition] + "-" +
                        tokens[CsvTransformerConfig.orderMonthColumnPosition] + "-" +
                        tokens[CsvTransformerConfig.orderDateColumnPosition]);
        String productId = tokens[CsvTransformerConfig.productIdColumnPosition];
        String productName = WordUtils.capitalizeFully(tokens[CsvTransformerConfig.productNameColumnPosition]); // give product name a proper case.
        BigDecimal quantity = new BigDecimal(tokens[CsvTransformerConfig.quantityColumnPosition].replaceAll("\"|,", ""));
        String unit = CsvTransformerConfig.transformedDataUnit;

        transformedData.append(orderId + ",");
        transformedData.append(orderDate + ",");
        transformedData.append(productId + ",");
        transformedData.append(productName + ",");
        transformedData.append(quantity + ",");
        transformedData.append(unit);

        return transformedData.toString();
    }
}