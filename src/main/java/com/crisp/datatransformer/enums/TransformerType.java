package com.crisp.datatransformer.enums;

/**
 * Enum to describes possible transformer type.
 */
public enum TransformerType {

    CSV("CSV");

    private String name;

    TransformerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
