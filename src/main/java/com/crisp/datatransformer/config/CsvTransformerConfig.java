package com.crisp.datatransformer.config;

import java.io.InputStream;
import java.util.Properties;

/**
 * Class to handle configuration for Csv Transformer.
 * This will be the class where all the configuration is handled.
 */
public class CsvTransformerConfig {

    /** Column position for order id. */
    public static int orderIdColumnPosition;

    /** Column position for order year. */
    public static int orderYearColumnPosition;

    /** Column position for order month. */
    public static int orderMonthColumnPosition;

    /** Column position for order date. */
    public static int orderDateColumnPosition;

    /** Column position for product id. */
    public static int productIdColumnPosition;

    /** Column position for product name. */
    public static int productNameColumnPosition;

    /** Column position for quantity. */
    public static int quantityColumnPosition;

    /** Default unit used for transformed data. */
    public static String transformedDataUnit;

    /** Header for the CSV output file. */
    public static String headerText;

    static {
        String resourceName = "csv_transformer_app.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties props = new Properties();
        try(InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
            props.load(resourceStream);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        orderIdColumnPosition = Integer.valueOf(props.getProperty("columnPosition.orderId"));
        String[] orderDateColumnPositions =
                props.getProperty("columnPosition.orderDate").split(",");
        orderYearColumnPosition = Integer.valueOf(orderDateColumnPositions[0]);
        orderMonthColumnPosition = Integer.valueOf(orderDateColumnPositions[1]);
        orderDateColumnPosition = Integer.valueOf(orderDateColumnPositions[2]);
        productIdColumnPosition = Integer.valueOf(props.getProperty("columnPosition.productId"));
        productNameColumnPosition = Integer.valueOf(props.getProperty("columnPosition.productName"));
        quantityColumnPosition= Integer.valueOf(props.getProperty("columnPosition.quantity"));
        transformedDataUnit = props.getProperty("transformedData.unit");
        headerText = props.getProperty("transformedData.headerText");
    }

}
