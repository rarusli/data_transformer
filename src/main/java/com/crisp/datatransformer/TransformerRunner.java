package com.crisp.datatransformer;

import com.crisp.datatransformer.config.CsvTransformerConfig;
import com.crisp.datatransformer.enums.TransformerType;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Main class to run the transformer to transform input to transformed output.
 */
public class TransformerRunner {

    /**
     * Transformer Factory object.
     */
    private static TransformerFactory transformerFactory = new TransformerFactory();

    /**
     * Take in input file from user and return two outputs : output.csv and error_output.csv in the
     * same folder of the input file name location.
     *
     * @param args  args[0] should be the input file name location.
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            try {
                String inputFileName = args[0];

                // If run sample and then just get from sample/sample1.csv
                if (inputFileName.equals("runsample")) {
                    inputFileName = "sample/sample1.csv";
                }

                readFile(inputFileName);
            } catch (Exception ex) {
                System.err.println("Issue when processing argument" + ex);
                System.exit(1);
            }
        } else {
            System.err.println(
                    "Usage : mvn exec:java -Dexec.mainClass=com.crisp.datatransformer.TransformerRunner " +
                            "-Dexec.args=[input_file_name_location]");
            System.exit(1);
        }
    }

    /**
     * Read input from user and transform the data.
     *
     * @param filename  Input filename to be transformed.
     */
    public static void readFile(String filename) {

        // Currently we choose CsvTransformer by default.
        Transformer transformer = transformerFactory.getDataTransformer(TransformerType.CSV.getName());
        String outputHeader = CsvTransformerConfig.headerText;

        // Generate output.csv and error_output.csv in the same
        File inputFile = new File(filename);
        String outputFilename = inputFile.getParent() + "/output.csv";
        String errorOutputFilename = inputFile.getParent() + "/error_output.txt";

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile));
             BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilename));
             BufferedWriter errorWriter = new BufferedWriter(new FileWriter(errorOutputFilename))) {

            String line = "";
            reader.readLine(); // skip the title.
            outputWriter.write( outputHeader + "\n");

            // Counters to display to user.
            int totalDataProcessed = 0;
            int totalError = 0;

            while ((line = reader.readLine()) != null) {
                try {
                    String output = transformer.transformData(line);
                    outputWriter.write(output + "\n");
                    totalDataProcessed++;
                } catch (Exception ex) {
                    String errorMessage = "Issue with this data in input '" + line + "'," + ex;
                    System.err.println(errorMessage);
                    errorWriter.write(errorMessage + "\n");
                    totalError++;
                }
            }

            System.out.println("Processed " + totalDataProcessed + " data with # of errors " + totalError);
        } catch (Exception ex) {
            System.err.println("Issue when reading file " + filename + " :" + ex);
            ex.printStackTrace();
        }
    }
}