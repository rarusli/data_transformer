package com.crisp.datatransformer;

import com.crisp.datatransformer.enums.TransformerType;

/**
 * Factory class that is responsible to return which Transformer to use.
 */
public class TransformerFactory {

    public Transformer transformer;

    /**
     * Return data transformer.
     *
     * @param dataTransformerType   Data transformer type that user requests.
     * @return the correct transformer based on user's request.
     */
    public Transformer getDataTransformer(String dataTransformerType){
        if(dataTransformerType == null){
            return null;
        }
        if(dataTransformerType.equalsIgnoreCase(TransformerType.CSV.getName())){
            transformer = new CsvTransformer();
        }

        return transformer;
    }

}
