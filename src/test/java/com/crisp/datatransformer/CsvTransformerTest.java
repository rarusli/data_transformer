package com.crisp.datatransformer;

import com.crisp.datatransformer.enums.TransformerType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test suite to run CSV Transformer.
 */
public class CsvTransformerTest {

    private static TransformerFactory transformerFactory;

    @BeforeAll
    public static void setup() {
        transformerFactory = new TransformerFactory();
    }

    /**
     * Test regular data transformation.
     */
    @Test
    public void testDataTransformation() {
        Transformer transformer = transformerFactory.getDataTransformer(TransformerType.CSV.getName());
        String input = "1000,2018,1,1,P-10001,Arugola,5250.50,Lorem,Ipsum,";
        String expectedOutput = "1000,Mon Jan 01 00:00:00 PST 2018,P-10001,Arugola,5250.50,kg";
        try {
            String output = transformer.transformData(input);
            assertEquals(output, expectedOutput);
        } catch(Exception ex) { ex.printStackTrace(); }
    }

    /**
     *
     * Test data with unpropered case product name. Our returned data should return us propered case product name.
     */
    @Test
    public void testDataTransformationWithUnproperedCase() {
        Transformer transformer = transformerFactory.getDataTransformer(TransformerType.CSV.getName());
        String input = "1001,2017,12,12,P-10002,IceBErg lettuce,500.00,Lorem,Ipsum,";
        String expectedOutput = "1001,Tue Dec 12 00:00:00 PST 2017,P-10002,Iceberg Lettuce,500.00,kg";
        try {
            String output = transformer.transformData(input);
            assertEquals(output, expectedOutput);
        } catch(Exception ex) { ex.printStackTrace(); }
    }

    /**
     * Test incomplete data. We are expecting exceptions.
     */
    @Test
    public void testIncompleteData() {
        Transformer transformer = transformerFactory.getDataTransformer(TransformerType.CSV.getName());
        String input = "1001,2017,12,12,P-10002,";

        // Expecting exception to be thrown.
        assertThrows(Exception.class, () -> transformer.transformData(input), "Error");
    }

    /**
     * Test data with special characters for example in product quantity:
     * quote around product quantity, or comma in the number.
     */
    @Test
    public void testProductQuantityWithSpecialChars() {
        Transformer transformer = transformerFactory.getDataTransformer(TransformerType.CSV.getName());
        String input = "1001,2017,12,12,P-10002,Iceberg Lettuce,\"5,100.00\",kg";
        String expectedOutput = "1001,Tue Dec 12 00:00:00 PST 2017,P-10002,Iceberg Lettuce,5100.00,kg";

        try {
            String output = transformer.transformData(input);
            assertEquals(output, expectedOutput);
        } catch(Exception ex) { ex.printStackTrace(); }

    }

}
