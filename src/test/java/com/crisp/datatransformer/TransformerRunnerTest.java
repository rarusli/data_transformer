package com.crisp.datatransformer;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test suite to run TransformerRunner.
 */
public class TransformerRunnerTest {

    /**
     * Test regular data transformation.
     * We are expecting output.csv and error_output.txt is created
     */
    @Test
    public void testDataTransformation() {
        TransformerRunner.readFile("sample/sample1.csv");

        boolean outputFileExists = new File("sample/output.csv").exists();
        assertTrue(outputFileExists);
        boolean errorOutputExists = new File("sample/error_output.txt").exists();
        assertTrue(errorOutputExists);
    }

    /**
     * Cleanup the output files before and after : output.csv and error_output.txt
     * to make sure we start test with a clean slate.
     */
    @BeforeAll
    @AfterAll
    public static void initialize() {
        new File("sample/output.csv").delete();
        new File("sample/error_output.txt").delete();
    }

}
